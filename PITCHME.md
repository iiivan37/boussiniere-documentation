# Boussinière Gîte de groupe

## Utilisation du système de réservation

---

Se connecter à l'interface d'administration afin d'accèder au tableau de bord
---
![Connexion](img/connexion.png)
---
![Dashboard](img/dashboard.png)
---
Cliquer sur **Pintpoint Booking System** dans la barre latérale à gauche
---
![Pintpoint Booking System](img/dashboard-step1.png)
---
Toujours dans la barre latérale, un sous-menu est apparu.
---

Cliquer sur **Calendars**
---
![Calendars](img/dashboard-step2.png)
---
Cliquer sur **Calendrier Boussinière** dans la fenêtre principale
---
![](img/dashboard-step3.png)
---
Sélectionner la période voulue sur le Calendrier
![](img/dashboard-step4.png)
---
Modifier l'état dans le champ nommé **status** dans la barre latérale droite
---
![](img/dashboard-step5.png)
---
Les changements sont enregistrés automatiquement. :)
